from math import floor

def parent(index):
    """
    Implement the 'Parent' algorithm from the lecture
    Please note that python arrays begin at index zero!
    :param index: the child index
    :return:the parent index
    """
    return int(floor((index + 1) / 2)) - 1


def left(index):
    """
    Implementation of the 'Left' algorithm from the lecture
    Please note that python arrays begin at index zero!
    :param index: the parent index
    :return: the left child index
    """
    return index * 2 + 1


def right(index):
    """
    Implement the 'Right' algorithm from the lecture
    Please note that python arrays begin at index zero!
    :param index: the parent index
    :return: the right child index
    """
    return index * 2 + 2


def max_heapify(array, index):
    """
    Implement the 'Max-Heapify' algorithm from the lecture
    Please note that python arrays begin at index zero!
    You may assume that the 'heap-size' equals the array length

    To swap two items you can use: array[i], array[j] = array[j], array[i]
    :param: the partially heapified array
    :index: the index to check
    """
    l = left(index)
    r = right(index)
    if l < len(array) and array[l] > array[index]:
        largest = l
    else:
        largest = index
    if r < len(array) and array[r] > array[largest]:
        largest = r
    if largest != index:
        array[largest], array[index] = array[index], array[largest]
        max_heapify(array, largest)


def build_max_heap(array):
    """
    Implement the 'Build-Max-Heap' algorithm from the lecture
    Please note that python arrays begin at index zero!
    :param array: the unsorted array
    :return: Nothing. Heapify inplace!
    """
    for i in xrange(int(floor(len(array) / 2)) - 1, -1, -1):
        max_heapify(array, i)


def heap_top_max(heap):
    """
    Implement the 'Heap-Maximum' algorithm from the lecture
    Please note that python arrays begin at index zero!
    This must not change the heap!
    You should check first whether the heap is empty or not!
    :param heap: the heap
    :return: the maximum element
    """
    return heap[0]


def heap_pop_max(heap):
    """
    Implement the 'Heap-Extract-Max' algorithm from the lecture
    Please note that python arrays begin at index zero!
    :param heap: the heap
    :return: the same as heap_top(heap)
    """
    max = heap[0]
    heap[0] = heap.pop()
    max_heapify(heap, 0)
    return max


def is_heap(array):
    """
    Implement this!
    Checks if the array is a heap
    Check for each element whether its parent is less than or equal to the element
    Hint: Take a look at the built-in functions 'any' and 'all'
    :param array: an array which may be a heap
    :return: True if array is a heap, False otherwise
    """
    return all(array[parent(i)] >= array[i] for i in xrange(1, len(array)))


if __name__ == '__main__':
    from random import shuffle

    numbers = range(15)
    shuffle(numbers)

    build_max_heap(numbers)
    print numbers
    print "That is a heap:", is_heap(numbers)
    print "The maximum element is 14:", heap_top_max(numbers) == 14
    heap_pop_max(numbers)
    print "After taking away the maximum element. 'numbers' is still a heap:", is_heap(numbers)
    print "But its length has decreased by 1:", len(numbers) == 14

    print "range(10) is not a heap:", not is_heap(range(10))