# -*- coding: utf-8 -*-
"""
Created on Mon May 11 10:38:50 2015

@author: mgutsche
"""

class Node(object):
	def __init__(self):
		self.left = None
		self.right = None
		self.parent = None
		self.counter = 0 # occurences of the string in the node
		self.content = None # string content

	def __str__(self):
		return self.content + " " + str(self.counter)


class BinTree(object):

	def	 __init__(self):
		self.root = None

	def __str__(self):
		return self.__recursive_print(self.root)

	def __recursive_print(self, node):
		if node is None:
			return ' '
		return self.__recursive_print(node.left) + '\n' + str(node) + ' '  + self.__recursive_print(node.right)


	def insert(self, key):
		'''
		Insert a key into the tree
		If the key is already present, it's counter is increased by 1.
		Otherwise, a new node is added and it's counter set to 1.
		:param key: String to be inserted
		'''
		mynode = self.findNode(key,self.root)
		if mynode is not None:
			mynode.counter += 1
		else:
			newnode = Node()
			newnode.content = key
			newnode.counter = 1
			if self.root is None:
				self.root = newnode
			else:
				parent = self.insertNode(newnode,self.root)
				newnode.parent = parent
			
	def insertNode(self,node,parent):
		'''
		Inserts key as a node below parent
		'''
		if (node.content < parent.content):
			if (parent.left is None):
				parent.left = node
			else:
				return self.insertNode(node,parent.left)
		else:
			if (parent.right is None):
				parent.right = node
			else:
				return self.insertNode(node,parent.right)
		return parent
		
	def find(self, key):
		'''
		Check if a key is present in the tree
		:param key: the string to be found
		:return: counter of the key's node if it is found, 0 otherwise
		'''
		mynode = self.findNode(key, self.root)
		if mynode is None:
			return 0
		else:
			return mynode.counter
		
	def findNode(self,key, node):
		'''returns node with key "key"'''
		if node is None:
			return None
		elif (key == node.content):
			return node
		elif (key < node.content):
			return self.findNode(key,node.left)
		else: #if (key > node.content):
			return self.findNode(key, node.right)
		

	def remove(self, key):
		'''
		Removes a key from the tree if it is present
		:param key: String to be removed
		:return: true if the key was found and removed, false otherwise
		'''
		mynode = self.findNode(key, self.root)
		if mynode is None:
			return False
		else:
			self.removeNode(mynode)
			return True
	
	def removeNode(self, node):
		if node.left is None:
			self.transplantNode(node, node.right)
		elif node.right is None:
			self.transplantNode(node, node.left)
		else:
			leftmost = self.leftmostNode(node.right)
			if node.parent is not node:
				self.transplantNode(leftmost, leftmost.right)
				leftmost.right = node.right
				leftmost.right.parent = leftmost
			self.transplantNode(node, leftmost)
			leftmost.left = node.left
			leftmost.left.parent = leftmost
	
	def transplantNode(self, oldNode, newNode):
		if oldNode.parent is None:
			self.root = newNode
		elif oldNode is oldNode.parent.left:
			oldNode.parent.left = newNode
		else: #if oldNode is oldNode.parent.right:
			oldNode.parent.right = newNode
		if newNode is not None:
			newNode.parent = oldNode.parent
			
	def leftmostNode(self, node):
		if node.left is None:
			return node
		else:
			return self.leftmostNode(node.left)


import re

def file_to_list(filename):
	'''
	Removes any special characters.
	:param: filename
	:return: list of words
	'''
	file = open(filename, 'r')
	text = file.read()
	file.close()
	text = re.sub('[^a-zA-Z]+', " ", text)
	words = list(text.split())
	return words

if __name__ == '__main__':
	words = ['Ente', 'Ente', 'Ente', 'Hase', 'Bier', 'Vogel', 'Karotte', 'Karotte', 'Ziegel', 'Aas', 'Ameise'] #small list for testing
	words = file_to_list('aroundTheWorldIn80Days.txt') # big list for testing
	btree = BinTree()
	for word in words:
		btree.insert(word)
	print btree
	single_word = 'you'
	print "Word '{}' occured {} times.".format(single_word, btree.find(single_word) )
	btree.remove(single_word)
	print "After removing it occours only {} times.".format( btree.find(single_word) )
