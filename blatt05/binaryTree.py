# -*- coding: utf-8 -*-
"""
Created on Sun May 24 16:00:00 2015

@author: mgutsche, dstoll
"""

class Node(object):
    def __init__(self):
        self.left = None
        self.right = None
        self.parent = None
        self.counter = 0 # occurences of the string in the node
        self.elementsOfSubtree = 0
        self.content = None # string content

    def __str__(self):
        return self.content + " " + str(self.counter)


class BinTree(object):

    def  __init__(self):
        self.root = None        

    def __str__(self):
        return self.__recursive_print(self.root)

    def __recursive_print(self, node):
        if node is None:
            return ' '
        return self.__recursive_print(node.left) + '\n' + str(node) + ' '  + self.__recursive_print(node.right)

    def insert(self, key):
        '''
        Insert a key into the tree
        If the key is already present, it's counter is increased by 1.
        Otherwise, a new node is added and it's counter set to 1.
        :param key: String to be inserted
        '''
        if self.root is None:
            self.root = self.__make_node(key)
        else:
            self.__recursive_insert(key, self.root)

    def __make_node(self, key):
        node = Node()
        node.counter += 1
        node.elementsOfSubtree += 1
        node.content = key
        return node

    def __recursive_insert(self, key, node):
        node.elementsOfSubtree +=1
        if key < node.content:
            if node.left is None:
                node.left = self.__make_node(key)
                node.left.parent = node
            else:
                self.__recursive_insert(key, node.left)
        elif key > node.content:
            if node.right is None:
                node.right = self.__make_node(key)
                node.right.parent = node
            else:
                self.__recursive_insert(key, node.right)
        else:
            node.counter += 1

    def find(self, key):
        '''
        Check if a key is present in the tree
        :param key: the string to be found
        :return: counter of the key's node if it is found, 0 otherwise
        '''
        node = self.__recursive_find(key, self.root)
        if node is None:
            return 0
        return node.counter

    def __recursive_find(self,key, node):
        if node == None:
            return None
        elif key < node.content:
            return self.__recursive_find(key, node.left)
        elif key > node.content:
            return self.__recursive_find(key, node.right)
        else:
            return node

    def getRankOfNode(self,key):
        rank = self.__recursive_rank(key, self.root)
        return (rank + 1, rank + self.find(key))
    
    def __recursive_rank(self,key, node):
        if key < node.content:
            return self.__recursive_rank(key, node.left)
        elif key > node.content:
            rank = node.counter + self.__recursive_rank(key, node.right)
            if node.left is not None:
                rank += node.left.elementsOfSubtree
            return rank
        elif node.left is not None:
            return node.left.elementsOfSubtree
        else:
            return 0
    
    def getSize(self):
        return self.__recursive_size(self.root)
        
    def __recursive_size(self,node):
        if node is None:
            return 0
        else:
            return self.__recursive_size(node.left) + self.__recursive_size(node.right) + 1
        
    def getDepth(self):
        return self.__recursive_depth(self.root)
        
    def __recursive_depth(self,node):
        if node is None:
            return 0
        else:
            return 1 + max(self.__recursive_depth(node.left), self.__recursive_depth(node.right))
    
import re

def file_to_list(filename):
    '''
    Removes any special characters.
    :param: filename
    :return: list of words
    '''
    file = open(filename, 'r')
    text = file.read()
    file.close()
    text = re.sub('[^a-zA-Z]+', " ", text)
    words = list(text.split())
    return words

if __name__ == '__main__':
    words = file_to_list('aroundTheWorldIn80Days.txt')
    btree = BinTree()
    for word in words:
        btree.insert(word)
    print btree
    print
    print 'Size: {}'.format(btree.getSize())
    print 'Depth: {}'.format(btree.getDepth())
    print 'Rank of "you": {}'.format(btree.getRankOfNode('you'))
    




