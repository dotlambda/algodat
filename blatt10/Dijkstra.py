# -*- coding: utf-8 -*-
 
def Dijkstra(source, dest):
    pass # remove this "pass", when entering your code
    #write your test function here
 
 
if __name__ == '__main__': 
    Dijkstra()
    #for testing purposes, you can use the following example.
    #Dijkstra(0,11) #should return "Gewicht: " 47, "Pfad: "[0,1,4,7,6,11] 
    #Note: the tutors will use own testcases to verify your Code!

graph = [
[None,14,None,None,None,None,None,None,None,19,None,None,None,None,None,None,None,None,None,None,None,None,None,None],
[None,None,8,None,6,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None],
[None,7,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None],
[None,None,None,None,11,None,None,None,None,None,6,None,None,None,None,None,None,None,None,None,None,None,None,None],
[None,11,None,14,None,15,None,7,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None],
[None,None,None,None,19,None,None,None,None,None,None,None,None,4,None,None,None,None,None,None,None,None,None,None],
[None,None,None,None,None,None,None,18,None,None,None,11,None,None,None,None,None,None,None,None,None,None,None,None],
[None,None,None,None,15,None,9,None,10,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None],
[None,None,None,None,None,None,None,19,None,None,None,None,10,None,None,None,None,None,None,None,None,None,None,None],
[14,None,None,None,None,None,None,None,None,None,20,None,None,None,None,None,None,None,None,None,None,12,None,None],
[None,None,None,3,None,None,None,None,None,None,None,None,None,None,None,None,None,None,3,None,None,None,None,None],
[None,None,None,None,None,None,17,None,None,None,4,None,None,None,None,13,None,None,None,None,None,None,None,None],
[None,None,None,None,None,None,None,None,3,None,None,None,None,20,None,None,None,12,None,None,None,None,None,None],
[None,None,None,None,None,15,None,None,None,None,None,None,14,None,None,None,None,None,None,None,11,None,None,None],
[None,None,7,None,None,None,None,None,None,None,None,None,None,14,None,None,None,None,None,None,None,None,None,8],
[None,None,None,None,None,None,None,None,None,None,None,10,None,None,None,None,2,None,None,None,None,None,None,None],
[None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,15,None,20,None,17,None,None,None,None],
[None,None,None,None,None,None,None,None,None,None,None,None,14,None,None,None,10,None,None,None,None,None,None,None],
[None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,7,None,None,None,None],
[None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,15,None,8,None,None,None,18,None],
[None,None,None,None,None,None,None,None,None,None,None,None,None,15,None,None,None,None,None,11,None,None,None,None],
[None,None,None,None,None,None,None,None,None,10,None,None,None,None,None,None,None,None,None,None,None,None,18,None],
[None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,8,None,5],
[None,None,None,None,None,None,None,None,None,None,None,None,None,None,1,None,None,None,None,None,None,None,20,None]
]