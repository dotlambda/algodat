b) analyseTime.py plotted alle drei Graphen nacheinander.
   Da bei ascendingList immer der best case gegeben ist, steigt der Graph linear an. (T(n)=Omega(n))
   Bei descendingList ist immer der worst case gegeben, deshalb steigt der Graph quadratisch an. (T(n)=Omega(n^2))
   Bei randomList tritt meistens ein Fall dazwischen ein. Deshalb kann man generell eine Tendenz T(n)=Omega(n^k) erkennen, wobei 1<k<2. Dieser Graph unterliegt deutlich größeren Scwankungen als die beiden anderen. Die liegt daran, dass eine zufällige Liste eben zufällig nah am worst/best case liegen kann und der Graph deshalb einen Ausschlag nach oben/unten bekommt.
   Dass auch die Graphen von ascendingList und descendingList nicht gleichmäßig verlaufen, sondern Ausschläge haben, liegt daran, dass nicht die gesamte Rechenkapazität für Python genutzt wird, sondern der Prozess immer wieder vom System unterbrochen wird. Außerdem muss der Interpreter z.B. manchmal garbage collection durchführen.

