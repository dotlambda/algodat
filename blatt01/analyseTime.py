import time
import random
import matplotlib.pyplot as plt
from InsertionSort import insertionSort #InsertionSort.py has to be in the same folder

class analyseTime:
    
    #def __init__(self):
        
    def getRandomList(self,n):
        '''returns an unsorted list with random values'''
        random.seed()
        list = []
        for i in range(n):
            list.append(random.randint(1,50))
            
        return list
    
    def getAscendingList(self, n):
        '''returns an ascended sorted list with random values'''
        list = self.getRandomList(n)
        list.sort()
        return list
        
    def getDescendingList(self,n):
        '''returns a descended sorted list with random values'''
        list = self.getRandomList(n)
        list.sort()
        list.reverse()
        return list

    def tic(self):
        '''returns current time in seconds'''
        self.timeTic = time.time()
        return self.timeTic

    def tac(self):
        '''returns time left since last tic() in seconds'''
        timeTac = time.time()
        return timeTac-self.timeTic

    def plot(self,n,timeList):
        '''creates and shows a plot of n teststeps and their corresponding values'''
        plt.plot(range(n),timeList)
        plt.ylabel('Time')
        plt.xlabel('Length of Input')
        plt.show()

# you can use the functions like this

# insert = insertionSort()
# analyse = analyseTime()
# numberOfTests = 200
# timeList = []
# for i in range(numberOfTests):
#     list = analyse.getRandomList(i)
#     start = analyse.tic()
#     list = insert.sort(list)
#     end = analyse.tac()
#     timeList.append(end)
# analyse.plot(numberOfTests,timeList)

'''Insert your code here'''

insert = insertionSort()
analyse = analyseTime()
numberOfTests = 200
timeList = []
for i in range(numberOfTests):
    list = analyse.getAscendingList(i)
    start = analyse.tic()
    list = insert.sort(list)
    end = analyse.tac()
    timeList.append(end)
analyse.plot(numberOfTests,timeList)

timeList = []
for i in range(numberOfTests):
    list = analyse.getRandomList(i)
    start = analyse.tic()
    list = insert.sort(list)
    end = analyse.tac()
    timeList.append(end)
analyse.plot(numberOfTests,timeList)

timeList = []
for i in range(numberOfTests):
    list = analyse.getDescendingList(i)
    start = analyse.tic()
    list = insert.sort(list)
    end = analyse.tac()
    timeList.append(end)
analyse.plot(numberOfTests,timeList)