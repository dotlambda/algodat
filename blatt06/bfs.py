# -*- coding: utf-8 -*-
"""
Author: akoenig
"""


class Vertex:
    def __init__(self,name, neighbors):
        self.name = name
        self.neighbours = neighbors
        self.visited = False
        self.distance = -1
        self.pi = None

class Graph(object):
    def __init__(self):
        self.neighbors = [[1,2,3],[0,3,4],[0,3,6,7],[0,1,2,4,5,6],[1,3,5,8],[3,4,6,7,8,10],[2,3,5,7],[2,5,6,10],[4,5,10,9],[8,10,11],[5,7,8,9,11],[9,10]]
        self.names = ['Hamburg','Bremen','Berlin','Hannover','Dortmund','Kassel','Leibzig','Dresden','Frankfurt','Stuttgart','Nuernberg','Muenchen']
        self.resetVertexes()
        
    def getIndexByName(self,name):
        return self.names.index(name)

    def resetVertexes(self):
        self.vertexes = []
        for i in range(0,len(self.names)):
            self.vertexes.append(Vertex(self.names[i], self.neighbors[i]))

from Queue import Queue

def applyBFS(graph,name):
    graph.resetVertexes()
    queue = Queue()
    start = graph.vertexes[graph.getIndexByName(name)]
    start.visited = True
    start.distance = 0
    queue.put(start)

    while not queue.empty():
        vertex = queue.get()
        for i in vertex.neighbours:
            neighbor = graph.vertexes[i]
            if not neighbor.visited:
                neighbor.visited = True
                neighbor.distance = vertex.distance + 1
                neighbor.pi = vertex
                queue.put(neighbor)

def getRoute(graph,start,end):
    applyBFS(graph,start)
    route = ""
    endIndex = graph.getIndexByName(end)
    vertex = graph.vertexes[endIndex]
    while vertex is not None:
        route = vertex.name + " => " + route
        vertex = vertex.pi
    return route[:-4]

#you can test your code with this
if __name__ == '__main__':
    graph = Graph()
    route = getRoute(graph,'Hamburg','Muenchen')
    print route
    