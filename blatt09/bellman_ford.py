# -*- coding: utf-8 -*-

def initialize(graph, source):
    d = {}
    p = {}
    for node in graph:
        d[node] = None
        p[node] = None
    d[source] = 0
    return d, p # d: distance, p: predessor

def relax(node, neighbour, graph, d, p):
    if d[node] is not None and (d[neighbour] is None \
    or d[neighbour] > d[node] + graph[node][neighbour]):
        d[neighbour] = d[node] + graph[node][neighbour]
        p[neighbour] = node

def bellman_ford(graph, source):
    d, p = initialize(graph, source)
    for i in xrange(len(graph) - 1):
        for node, neighbours in graph.iteritems():
            for neighbour in neighbours:
                relax(node, neighbour, graph, d, p)
    for node, neighbours in graph.iteritems():
        for neighbour in neighbours:
            if d[node] is not None and (d[neighbour] is None \
            or d[neighbour] > d[node] + graph[node][neighbour]):
                raise ValueError('Graph contains a negative-weight cycle')
    return d, p

def test():
    graph = { \
        'a': {'d': -1, 'c':  4}, \
        'b': {'e':  3, 'd':  2, 'e':  2}, \
        'c': {'b':  1}, \
        'd': {'b':  1, 'c':  5}, \
        'e': {'d': -3} \
        }
    d, p = bellman_ford(graph, 'a')
    assert d['b'] == 0
    assert p['b'] == 'd'
    d, p = bellman_ford(graph, 'b')
    assert d['d'] == -1
    assert p['a'] == None
    error_graph = { \
        'a': {'d': -2, 'c':  4}, \
        'b': {'e':  3, 'd':  2, 'e':  2}, \
        'c': {'b':  1}, \
        'd': {'b':  1, 'c':  5, 'a': 1}, \
        'e': {'d': -3} \
        }
    try:
        bellman_ford(error_graph, 'a')
    except Exception, e:
        assert type(e) is ValueError
    else:
        assert 0 == 1

if __name__ == '__main__':
    import unittest
    test()
