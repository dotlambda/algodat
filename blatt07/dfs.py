# -*- coding: utf-8 -*-
"""
Author: twiese
"""


class Graph(object):
    def __init__(self):
        self.names = ['a','b','c','d','e','f','g','h','i']
        self.adj = [['b'], ['e'], ['i'], ['e', 'g'], [], ['d', 'g'], ['b'], ['c'], ['d', 'h']]
        self.d = []
        self.f = []
        self.pi = []
        self.visited = []
        for i in range(len(self.names)):
            self.visited.append(False)
            self.pi.append(None)
            self.d.append(None)
            self.f.append(None)

    def getIndexByName(self,name):
        return self.names.index(name)

    def getBeginByName(self,name):
        return self.d[self.getIndexByName(name)]

    def getEndByName(self,name):
         return self.f[self.getIndexByName(name)]

    def getAdjacents(self,name):
        return self.adj[self.getIndexByName(name)]

def DFS(graph, name):
    global time
    time = 0
    DFSvisit(graph, name)
    for i in range(len(graph.names)):
        if not graph.visited[i]:
            DFSvisit(graph, graph.names[i])
    ret = ""
    for n in graph.names:
        ret += "'{}': {} - {}\n".format(n, graph.getBeginByName(n), graph.getEndByName(n))
    return ret

def DFSvisit(graph, next):
    global time
    u = graph.getIndexByName(next)
    time += 1
    graph.d[u] = time
    graph.visited[u] = True
    for nextNext in graph.adj[u]:
        v = graph.getIndexByName(nextNext)
        if not graph.visited[v]:
            graph.pi[v] = next
            DFSvisit(graph, nextNext)
    time += 1
    graph.f[u] = time

#you can test your code with this
if __name__ == '__main__':
    graph = Graph()
    DFSValues = DFS(graph, 'c') #DFSValues are d an f
    print DFSValues
