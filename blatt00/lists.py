from math import pi


def create_list(number):
    """
    Creates a list. Each digit of the number should be one element of
    the new list.
    E.g. create_list(3.1415) => [3, 1, 4, 1, 5]
    
    Note: You may ignore this case:
        create_list(6.022E-23)
    
    :returns: a list of integers
    """

    lst = []
    i = 0
    remainder = number
    while remainder != 0:
        lst.append(int(remainder))
        i += 1
        remainder = number * (10 ** i) % 10
    return lst

def invert_list(alist):
    """
    Inverts the list in-place. I.e. dont't return a copy.
    Just modify the parameter.
    
    Note: If you read the official python tutorial this
    task should be really easy.
    
    E.g. [1, 2, 3] -> invert_list -> [3, 2, 1]
    :param alist: the list you want to modify
    :type alist: list
    :returns: nothing
    """

    length = len(alist)
    to_swap = range(0, length / 2)
    for i in to_swap:
        j = length - 1 - i
        alist[i], alist[j] = alist[j], alist[i]  # swap two elements


if __name__ == "__main__":
    numbers = create_list(pi)
    
    print numbers

    invert_list(numbers)
    
    print numbers   