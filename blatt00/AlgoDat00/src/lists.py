from math import pi


def create_list(number):
    """
    Creates a list. Each digit of the number should be one element of
    the new list.
    E.g. create_list(3.1415) => [3, 1, 4, 1, 5]
    
    Note: You may ignore this case:
        create_list(6.022E-23)
    
    :returns: a list of integers
    """
    # This is a dummy implementation.
    # Remove it first and add your own code
    return range(1, 11)
    
    # TODO: Your code goes here!


def invert_list(alist):
    """
    Inverts the list in-place. I.e. dont't return a copy.
    Just modify the parameter.
    
    Note: If you read the official python tutorial this
    task should be really easy.
    
    E.g. [1, 2, 3] -> invert_list -> [3, 2, 1]
    :param alist: the list you want to modify
    :type alist: list
    :returns: nothing
    """
    
    # TODO: Your code goes here!


if __name__ == "__main__":
    numbers = create_list(pi)
    
    print numbers
    
    invert_list(numbers)
    
    print numbers   