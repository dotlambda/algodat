QUOTE = """Debugging is twice as hard as writing the code in the first place.
Therefore, if you write the code as cleverly as possible,
you are, by definition, not smart enough to debug it."""


def count_words(content):
    """
    Print each word in content and
    count how many words start with an upper case letter and how many
    start with a lower case letter
    
    :param content: the words to print and count
    :returns: upper count, lower count
    """
    upper = 0
    lower = 0
    
    # TODO: Your code goes here!
    
    return upper, lower


if __name__ == "__main__":
    upper, lower = count_words(QUOTE)
    
    print """There are {} words starting with an upper case letter and {}
    words starting with a lower case letter.
    """.format(upper, lower)