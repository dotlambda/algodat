# Import the QUOTE from string.py here
# TODO: Your code goes here!

def count_each_word(content):
    """
    Count the occurrence of each word and write it into a dict.
    E.g. count_each_word("abc def abc") =>
        { "abc": 2, "def": 1 }
    
    :param content: the words to count
    :returns: the dictionary, containing the words
    """
    words = {}
    
    # TODO: Your code goes here!
    
    return words


def most_common_word(words):
    """
    Find the most common word. I.e. the word with the largest value in the dict
    :param words: the dictionary of counted words
    :type words: dict
    :returns: the most common word, its number of occurrences
    """
    
    word = ""
    count = 0
    
    # TODO: Your code goes here
    
    return word, count

if __name__ == "__main__":
    # Import the quote from string.py
    words = count_each_word(QUOTE)
    
    word, count = most_common_word(words)
    
    print "The most common word is '{}'. It occurred {} times.".format(word, count)
    
    