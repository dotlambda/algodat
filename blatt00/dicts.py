# Import the QUOTE from string.py here
from strings import QUOTE

def count_each_word(content):
    """
    Count the occurrence of each word and write it into a dict.
    E.g. count_each_word("abc def abc") =>
        { "abc": 2, "def": 1 }
    
    :param content: the words to count
    :returns: the dictionary, containing the words
    """
    words = {}
    
    for word in content.split():
        if word in words:
            words[word] += 1
        else:
            words[word] = 1
    
    return words


def most_common_word(words):
    """
    Find the most common word. I.e. the word with the largest value in the dict
    :param words: the dictionary of counted words
    :type words: dict
    :returns: the most common word, its number of occurrences
    """
    
    word = ""
    count = 0
    
    for w, c in words.items():
        if c > count:
            count = c
            word = w
    
    return word, count

if __name__ == "__main__":
    # Import the quote from string.py
    words = count_each_word(QUOTE)
    
    word, count = most_common_word(words)
    
    print "The most common word is '{}'. It occurred {} times.".format(word, count)
    
    